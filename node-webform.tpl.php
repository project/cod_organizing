<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">
 <?php print $conference_header ?>  
  <?php if ($webform): ?>
    <?php print $webform ?>
  <?php else: ?>
    <div class="scheduled-sessions">
      <?php print $cod_scheduled_sessions; ?>
    </div>
    <div class="proposed-sessions">
      <?php print $cod_proposed_sessions; ?>  
    </div>
  <?php endif; ?>
</div>
