<?php
/*
 * Declare the available regions implemented by this engine.
 *
 * @return
 *    An array of regions.  The first array element will be used as the default region for themes.
 *    Each array element takes the format: variable_name => t('human readable name')
 */
function cod_organizing_regions() {
  return array(
       'left' => t('left sidebar'),
       'right' => t('right sidebar'),
       'content_top' => t('content top'),
       'content_bottom' => t('content bottom'),
       'header' => t('header'),
       'footer' => t('footer')
  );
} 

/**
 * Intercept template variables
 *
 * @param $hook
 *   The name of the theme function being executed
 * @param $vars
 *   A sequential array of variables passed to the theme function.
 */

function _phptemplate_variables($hook, $vars = array()) {
  drupal_add_js(path_to_theme() . '/misc/jquery.corner.js');
  drupal_add_js(path_to_theme() . '/misc/cod.corner.js');
  switch ($hook) {
    case 'page':
      global $user;
      $vars['body_classes'] = cod_organizing_body_classes($vars);
      if ($user->uid) {
        $vars['top_links'] = t("Welcome, !user.", array('!user' => theme('username', $user))). ' | '. l('my account', "user/$user->uid/edit") .' | '. l('logout','logout');
      }
      else {
        if (variable_get('user_register', 1) == 0) {
          $vars['top_links'] = t('Already Registered? Click here to <a href="!login">login</a>.', array('!login' => url('user/login')));
        }
        else {
          $vars['top_links'] = t('We encourage you to <a href="!login">log in</a> or <a href="!register">register</a>.', array('!login' => url('user/login'), '!register' => url('user/register')));
        }
      }
      // unset breadcrumb
      $vars['breadcrumb'] = '';
      // unset title for node page views
      if (isset($vars['node'])) {
        $vars['title'] = '';
      }
      // remove drupal tabs for conference node (us conference admin block instead)
      if ($vars['node']->type == 'webform' || (arg(0) == 'user' && is_numeric(arg(1)))) {
        $vars['tabs'] = '';
      }
      break;
      
    case 'node':
      // get the currently logged in user
      global $user;

      $vars['is_admin'] = in_array('admin', $user->roles);
      $vars['node_classes'] = cod_organizing_node_classes($vars); 
      
      // Here is the way to switch to a different node-<something> template based on node properties.
      $vars['template_files'] = $vars['page'] ? array('node-default-page', 'node-'. $vars['node']->type .'-page', 'node-'. $vars['node']->nid .'-page') : array('node-'. $vars['node']->nid);  
      
      // add extra variables to the templates
      switch($vars['node']->type) {
        case 'webform':
          $vars = cod_organizing_webform_vars($vars);
          break;
        case 'session':
          $vars = cod_organizing_session_vars($vars);
          break;
        case 'bio':      
          $vars = cod_organizing_bio_vars($vars);
          break;  
        case 'group':      
          $vars = cod_organizing_group_vars($vars);
          break;
        case 'notes':
          $vars = cod_organizing_notes_vars($vars);
          break;
      }

      break;
      
    case 'comment':
      // we load the node object that the current comment is attached to
      $node = node_load($vars['comment']->nid);
      $vars['author_comment'] = $vars['comment']->uid == $node->uid ? TRUE : FALSE;
      global $user;
      $account_id = ($vars['comment']->uid) ? $vars['comment']->uid : $user->uid;
      $account = user_load(array("uid" => $account_id));
      $vars['author_picture'] = theme('user_picture', $account);

      // Do not show comments if session is published
      if ($node->type == 'session' && $node->field_scheduled[0]['value'] == 1) { 
        $vars['content'] = '';
        $vars['author_picture'] = '';
        $vars['picture'] = '';
        $vars['links'] = '';
      }
      break;
    case 'block':
      break;
  }
  
  return $vars;
}

/**
 *  Add extra $vars to node-session.tpl.php
 */
function cod_organizing_session_vars($vars) {
  global $user;
  $group = conference_organizing_get_conference_context();
  
  // distinguish between scheduled and proposed sessions
  $vars['scheduled'] = isset($vars['node']->field_scheduled[0]['value']) ? TRUE : FALSE;

  // only show the voting widget for proposed sessions
  if (!$vars['scheduled']) {
    $vars['vote_up_down'] = $vars['node']->content['vote_up_down']['#value'];
  }
  $cnt = comment_num_all($vars['node']->nid);
  $vars['short_content'] = $vars['node']->content['body']['#value'];

  
  if (!$vars['page']) {
    $vars['links'] = '';
    $vars['read_more'] = '<div class="read-more-session">'. l(t('Read more'), 'node/'. $vars['node']->nid, array('class' => 'co-button')) .'</div>';
  }
  elseif ($vars['scheduled']) {
    // Related Discussions
    $vars['discussions_title'] = t('Related Discussions');
    $related_discussions = conference_organizing_get_related_discussions($vars['node']->nid);
    if (count($related_discussions) > 0) { 
      $vars['discussions_body'] = theme('item_list', $related_discussions);
    } else {
      $vars['discussions_body'] = t('There are no related discussions for this session.');
    }
    $vars['discussions_add_link'] = l(t('Start a Discussion'), 'node/add/discussion/parent/'.$group->nid.'/session/'.$vars['node']->nid, array('class' => 'co-button'), 'destination=node/'.$vars['node']->nid, null, false, true);

    // Session Links
    // TODO - make these functions theme functions we can call
    $vars['links_body'] = '<div id="sessions-links-content">' . conference_organizing_links_make_links_table($vars['node']->nid) . '</div>';
    $vars['links_title'] = t('Session Links');
    $vars['links_add'] = theme_conference_organizing_links_add_link();
    global $user;
    if (conference_organizing_access('can create session link',$group->nid, '', $user->uid)) {
      $vars['links_add_link'] = l(t('Add link'), 'node/add/link/parent/'.$group->nid.'/session/'.$vars['node']->nid, array('class' => 'co-button'), 'destination=node/'.$vars['node']->nid, null, false, true);
    }
    if (user_access('edit session content')) {
      $vars['assets_add_link'] = l(t('Add asset'), 'node/'.$vars['node']->nid.'/edit/assets', array('class' => 'co-button'), NULL, NULL, FALSE, TRUE);
    };
    
    // only do this work if it's a full page view
    if (($vars['node']->field_notes_node[0]['nid'] > 0) && ($notes = node_load($vars['node']->field_notes_node[0]['nid'])) && is_object($notes)) {
      // load up the notes node if one is present
      if (node_access('view', $notes)) {
        $notes = node_prepare($notes);
        $vars['notes_title'] = $notes->title;
        $vars['notes_body'] = $notes->body;
        $vars['notes_teaser'] = $notes->teaser;
        $vars['notes'] = $notes; // pass the node, just for kicks
        if (conference_organizing_access('edit notes', $group->nid, $vars['notes']->nid)) {
          $vars['notes_edit_link'] = l(t('Edit notes'), 'node/'. $notes->nid .'/edit', array('class' => 'co-button'),  drupal_get_destination(), null, false, true);
        }
      }
      else {
        $notes = '';
      }
      // These are some wierd asset checks, we should refactor
      // and whatever else we want...
    }
    else {
      $vars['notes_title'] = t('Session Notes');
      $vars['notes'] = TRUE;
      $vars['notes_body'] = t("No notes have been created yet.");
      // otherwise, pass an edit link if user created session or they have access to edit session content
      if (conference_organizing_access('create notes', $group->nid, $vars['node']->nid)) {
        $vars['notes_add_link'] = l(t('Add note'), 'node/add/notes/'. $vars['node']->nid, array('class' => 'co-button'), drupal_get_destination(), null, false, true);
      }
    }
  }
  
  // send scheduled session nodes to their own template
  if ($vars['scheduled'] && $vars['page']) {
    $vars['template_files'] = array('node-active_session-page');
    $start_time = $vars['node']->field_schedule_start_time[0]['value'];
    $start = format_date($start_time, 'custom', 'F j | g:i a');
    $location = $vars['node']->field_location[0]['value'];
    $info = array($start, $location);
    $vars['info'] = implode(" | ", $info);
    $vars['assets_title'] = t('Session Assets');
    $rows = array();
    foreach ($vars['node']->files as $file) {
      $file = (object)$file;
      if ($file->list && !$file->remove) {
        // Generate valid URL for both existing attachments and preview of new attachments (these have 'upload' in fid)
        $href = file_create_url((strpos($file->fid, 'upload') === FALSE ? $file->filepath : file_create_filename($file->filename, file_create_path())));
        $text = $file->description ? $file->description : $file->filename;
        $text .= ' ('. format_size($file->filesize) .')';
        $rows[] = l($text, $href);
      }
    }
    if (count($rows) > 0 && node_access('view', $vars['node']->nid)) { 
      $vars['attachments'] = theme('item_list', $rows);  
    } else { 
      $vars['attachments'] = t('No assets have been submitted for this session.');
    }
    $vars['notes_link'] = $vars['notes_edit_link'] ? $vars['notes_edit_link'] : $vars['notes_add_link'];
    if (module_exists('conference_organizing')) {
      $vars['session_description'] = theme('conference_organizing_toggle', truncate_utf8($vars['node']->content['body']['#value'], 80), $vars['node']->content['body']['#value']);
    }
  } 
  
  // handle connection to working groups if the session has ended
  // won't fire if "group" content-type is not enabled
  if($vars['node']->session_end_time[0]['value'] < time() && content_types('group')) {
    $node = db_fetch_object(db_query("SELECT n.nid FROM {node} n INNER JOIN {content_field_related_session} cf ON n.nid = cf.nid WHERE field_related_session_nid = %d and n.type = 'group'", $vars['node']->nid));
    if ($node->nid > 0) { // there already is a session
      $node = node_load($node->nid);
      $vars['working_group_title'] = $node->title;
      $vars['working_group_add_link'] = l(t('Visit working group'), 'node/'. $node->nid, array('class' => 'co-button'));
      $vars['working_group_body'] = $node->og_description;
      // do something here to display a summary of the group
    }
    else {
      $vars['working_group_title'] = t('Session working group');
      $vars['working_group_body'] = '<p>'. t('There is not yet a working group carrying on this session.') .'</p>';
      if ($user->uid == $vars['node']->uid || user_access('administer nodes')) {
        $vars['working_group_add_link'] = l(t('Create working group'), 'node/add/group/'. $vars['node']->nid, array('class' => 'co-button'), drupal_get_destination(), null, false, true);
      }
    }
  }
  
  return $vars;
} 

/**
 *  Add or modify $vars for node-webform.tpl.php
 */
function cod_organizing_webform_vars($vars) {
  global $user;
  global $base_url;
  // Load javascript for toggle divs and for rounded corners
  drupal_add_js(path_to_theme() .'/misc/cod_session.js');
  // conference info
  if (module_exists('conference_organizing')) {
    $group = conference_organizing_get_conference_context();
    // supress default og messages
    // TODO - make this happen elsewhere
    conference_organizing_supress_og_messages();
    // TRUE if form needs to be filled or if viewing a webform result

  }
  
  // $conferences is array of all conferences a user needs to register for
  $conferences = conference_organizing_check_reg($user->uid);
  if ((module_exists('conference_organizing') && $user->uid && is_object($conferences[$group->nid]) && $group->og_selective != 3) || is_numeric($_GET['sid'])) {
    $vars['webform'] = '<h2>'. t('Use this form to complete your conference registration') .'</h2>';
    $vars['webform'] .= $vars['node']->content['webform']['#value'];

    
    if (arg(2) == 'done') {
      drupal_set_message("Thank you for registering!");
      drupal_goto('node/'. $vars['node']->nid);
    }
  }
  else {
    // scheduled sessions view
    $view = views_get_view('cod_scheduled_sessions');
    $scheduled = views_build_view('embed', $view, array(0 => $group->nid));
    if ($scheduled) {
      $vars['cod_scheduled_sessions'] = '<h1 class="green-session-title featured-sessions-title">'. l('Featured sessions', 'schedule/'. $vars['node']->nid) .'</h1>';
      $vars['cod_scheduled_sessions'] .= $scheduled;
      $vars['cod_scheduled_sessions'] .= '<div class="view-all-confirmed-sessions">'.l("View All Sessions", 'schedule/'. $vars['node']->nid).'</div>';
    }
    if ($vars['node']->field_allow_session_proposals[0]['value']) {
      // proposed sessions view
      $view = views_get_view('cod_proposed_sessions');
      $proposed = views_build_view('embed', $view, array(0 => $group->nid), TRUE, 5);
      if ($proposed) {
        $vars['cod_proposed_sessions'] = '<h1 class="green-session-title proposed-sessions-title">'. l('Proposed sessions', 'proposed_sessions/'. $vars['node']->nid) .'</h1>';
        $vars['cod_proposed_sessions'] .= $proposed;
        $vars['cod_proposed_sessions'] .= '<div class="view-all-proposed-sessions">'.l("View Proposed Sessions", 'proposed_sessions/'. $vars['node']->nid).'</div>';        
      }
    }
  }
  return $vars;
}

/**
 *  Add or modify $vars for node-bio.tpl.php
 */
function cod_organizing_bio_vars($vars) {
  $node = $vars['node'];
  $account = user_load(array('uid' => $node->uid));
  $vars['picture'] = theme('user_picture', $account);
  $vars['user_name'] = theme('username', $account);
  $vars['first_name'] = $node->field_first_name[0]['value'];
  $vars['last_name'] = $node->field_last_name[0]['value'];
  $vars['organization'] = $node->field_organization[0]['value'];
  $vars['position'] = $node->field_title[0]['value'];
  $info = array();
  if ($vars['position']) { $info[] = $vars['position']; }
  if ($vars['organization']) { $info[] = $vars['organization']; }
  $vars['info'] = implode(' | ', $info);
  $vars['profile_title'] = t("$account->name's Profile");
  $vars['bio'] = $node->content['body']['#value'];
  
  if (module_exists('conference_organizing') && $vars['page'] == 1) {
    $view = views_get_view('cod_home');
    $view = views_build_view('items', &$view, $args = array(), $use_pager = false, $limit = 1, $page = 0);
    $items = $view['items'];
    $vars['my_schedule'] = conference_organizing_my_schedule($items[0]->nid);
  }
  return $vars;
}


/**
 *  Add or modify $vars for node-group.tpl.php file
 */
function cod_organizing_group_vars($vars) {
  $node = $vars['node'];
  $account = user_load(array('uid' => $node->uid));
  $group = og_get_group_context();
  conference_organizing_supress_og_messages();
  $notes_nid = $vars['node']->field_notes_node[0]['nid'];
  
  // if user is a group member or group isn't closed
  if (conference_organizing_access('group member', $group->nid) || $group->og_selective != 3) {
    // if there are notes already associated with working group
    if (is_numeric($notes_nid) && is_object(node_load($notes_nid))) {
      // load up the notes node if one is present
      $notes = node_load($notes_nid);
      if (node_access('view', $node)) {
        $notes = node_prepare($notes);
        $vars['notes_title'] = $notes->title;
        $vars['notes_body'] = $notes->body;
        $vars['notes_teaser'] = $notes->teaser;
        $vars['notes'] = $notes; // pass the node, just for kicks
        if (conference_organizing_access('edit notes', $group->nid, $vars['notes']->nid)) {
          $vars['notes_edit_link'] = l(t('Edit notes'), 'node/'. $notes->nid .'/edit', array('class' => 'co-button'),  drupal_get_destination(), null, false, true);
        }
      }
      else {
        $notes = '';
      }
    }
    else {
      $vars['notes_title'] = t('Session Notes');
      $vars['notes'] = TRUE;
      $vars['notes_body'] = t("No notes have been created yet.");
      // otherwise, pass an edit link if user created session or they have access to edit session content
      if (conference_organizing_access('create notes', $group->nid, $vars['node']->nid)) {
        $vars['notes_edit_link'] = l(t('Add notes'), 'node/add/notes/'. $vars['node']->nid, array('class' => 'co-button'),  drupal_get_destination(), null, false, true);
      }
    }
    
    if (conference_organizing_access('group member', $group->nid)) {
      $vars['message_post_link'] = l(t('Post message'), 'node/add/discussion/'. $vars['node']->nid, array('class' => 'co-button'));
    }

    // messages view
    $view = views_get_view('cod_messages');
    $messages = views_build_view('embed', $view, array(0 => $group->nid), TRUE, 3);
    if ($messages) {
      $vars['messages'] = $messages;
      $vars['messages_feed'] .= theme('feed_icon', 'messages/'. $group->nid .'/feed', t('RSS Feed'));
    }

    // query tags for tag cloud
    $result = db_query_range('SELECT COUNT(*) AS count, d.tid, d.name, d.vid FROM {term_data} d INNER JOIN {term_node} n ON d.tid = n.tid INNER JOIN {og_ancestry} og ON n.nid = og.nid WHERE og.group_nid = %d GROUP BY d.tid, d.name, d.vid ORDER BY count DESC', $group->nid, 0, 25);
    $vars['tag_cloud'] = theme('tagadelic_weighted', tagadelic_build_weighted_tags($result, 6));
  }
  else {
    $vars['notes_body'] = t("Sorry, you are not a member of this group.  Please contact the administrator to be added.");
  } 
  return $vars;
}

/**
 *  Add or modify $vars for node-group.tpl.php file
 */
function cod_organizing_notes_vars($vars) {
  $group = og_get_group_context();
  
  if (conference_organizing_access('edit notes', $group->nid, $vars['node']->nid)) {
    $vars['notes_edit_link'] = l(t('Edit'), 'node/'. $vars['node']->nid .'/edit', array('class' => 'co-button'),  drupal_get_destination(), null, false, true);
  }
  return $vars;
}

/**
 *  Add body classes
 */
function cod_organizing_body_classes($vars) {
  $body_classes = array();
  // classes for body element
  // allows advanced theming based on context (home page, node of certain type, etc.)
  $body_classes[] = ($vars['is_front']) ? 'front' : 'not-front';
  $body_classes[] = ($vars['logged_in']) ? 'logged-in' : 'not-logged-in';
  if ($vars['node']->type) {
    $body_classes[] = 'ntype-'. zen_id_safe($vars['node']->type);
  }
  switch (TRUE) {
    case $vars['sidebar_left'] && $vars['sidebar_right'] :
      $body_classes[] = 'both-sidebars';
      break;
    case $vars['sidebar_left'] :
      $body_classes[] = 'sidebar-left';
      break;
    case $vars['sidebar_right'] :
      $body_classes[] = 'sidebar-right';
      break;
  }
  if (arg(0) == 'groups_directory') {
    $body_classes[] = 'groups-directory';
  }
  if (arg(0) == 'schedule') {
    $body_classes[] = 'schedule';  
  }
  // implode with spaces
  return implode(' ', $body_classes);
} 

/**
 * Theme override of theme_filter_tips
 * http://api.drupal.org/api/5/function/theme_filter_tips
 */
function cod_organizing_filter_tips($tips, $long = FALSE, $extra = '') {
  // simplify instructions on node edit form
  if (arg(2) == 'edit' && !user_access('administer nodes')) {
    return '<p>'. t('Basic HTML tags are allowed (') . l(t('more info'), 'filter/tips', array('target' => '_blank')) . ')</p>';
  }
  else {
    $output = '';

    $multiple = count($tips) > 1;
    if ($multiple) {
      $output = t('input formats') .':';
    }

    if (count($tips)) {
      if ($multiple) {
        $output .= '<ul>';
      }
      foreach ($tips as $name => $tiplist) {
        if ($multiple) {
          $output .= '<li>';
          $output .= '<strong>'. $name .'</strong>:<br />';
        }

        $tips = '';
        foreach ($tiplist as $tip) {
          $tips .= '<li'. ($long ? ' id="filter-'. str_replace("/", "-", $tip['id']) .'">' : '>') . $tip['tip'] . '</li>';
        }

        if ($tips) {
          $output .= "<ul class=\"tips\">$tips</ul>";
        }

        if ($multiple) {
          $output .= '</li>';
        }
      }
      if ($multiple) {
        $output .= '</ul>';
      }
    }
    return $output;
  }
}

/**
 * Theme override of theme_filter_tips
 * http://api.drupal.org/api/5/function/theme_filter_tips_more_info
 */
function cod_organizing_filter_tips_more_info() {
  return;
}

/**
 *  Add node classes
 */
function cod_organizing_node_classes($vars) {
  $node_classes = array('node');
  if ($vars['sticky']) {
    $node_classes[] = 'sticky';
  }
  if (!$vars['node']->status) {
    $node_classes[] = 'node-unpublished';
  }
  // add a flag for scheduled session vs. non scheduled session node.
  $node_classes[] = isset($vars['node']->field_scheduled[0]['value']) ? 'scheduled-node' : 'proposed-node';
  $node_classes[] = 'ntype-'. zen_id_safe($vars['node']->type);
  return implode(' ', $node_classes);
}

/**
 * Change 'points' to 'votes' on rating widget
 */
function cod_organizing_vote_up_down_points_alt($cid, $type) {
  $vote_result = votingapi_get_voting_result($type, $cid, 'points', variable_get('vote_up_down_tag', 'vote'), 'sum');
  if ($vote_result) {
    $output = '<div id="vote_points_'. $cid .'" class="vote-points">'. $vote_result->value;
  }
  else {
    $output = '<div id="vote_points_'. $cid .'" class="vote-points">0';
  }

  $output .= '<div class="vote-points-label">'. t('votes') .'</div></div>';

  return $output;
}


/**
 *  Override tabs
 */
function cod_organizing_menu_local_tasks() {
  $output = '';
  $node = new stdClass();
  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));
  }
  if (in_array($node->type, array('notes'))) {
    return;
  }
  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">\n". $primary ."</ul>\n";
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= "<ul class=\"tabs secondary\">\n". $secondary ."</ul>\n";
  }

  return $output;
}


/******************* Helper functions *****************/

 
 
/**
* Converts a string to a suitable html ID attribute.
* - Preceeds initial numeric with 'n' character.
* - Replaces space and underscore with dash.
* - Converts entire string to lowercase.
* - Works for classes too!
* 
* @param string $string
*  the string
* @return
*  the converted string
*/
function zen_id_safe($string) {
  if (is_numeric($string{0})) {
    // if the first character is numeric, add 'n' in front
    $string = 'n'. $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

// Stuff Added During Cofnerence
function phptemplate_views_filters($form) {
  $view = $form['view']['#value'];

  foreach ($view->exposed_filter as $count => $expose) {
    $row[] = drupal_render($form["op$count"]) . drupal_render($form["filter$count"]);
    $label[] = $expose['label'];
  }
  $row[] = l('<input type="button" value="Reset">', $view->url.'/'.$view->args[0], array('onclick' => "location.href='/".$view->url.'/'.$view->args[0]."'"), NULL, NULL, NULL, TRUE);
  $label[] = '';
  $row[] = drupal_render($form['submit']);
  $label[] = ''; // so the column count is the same.

  // make the 'q' come first
  return drupal_render($form['q']) . theme('table', $label, array($row)) . drupal_render($form);
}


function phptemplate_imagefield_image($file, $alt = '', $title = '', $attributes = NULL, $getsize = TRUE) {
  $file = (array)$file;
  if (!$getsize || (is_file($file['filepath']) && (list($width, $height, $type, $image_attributes) = @getimagesize($file['filepath'])))) {
    $attributes = drupal_attributes($attributes);
    
    $path = $file['fid'] == 'upload' ? $file['preview'] : $file['filepath'];
    $alt = empty($alt) ? $file['alt'] : $alt;
    $title = empty($title) ? $file['title'] : $title;
    
    $url = file_create_url($path);
    $img = '<img src="'. check_url($url) .'" alt="'. check_plain($alt) .'" title="'. check_plain($title) .'" '. $image_attributes . $attributes .' />';
    // if we're on a view, lets return an array
    if (arg(0) == 'conferences') {
      return l($img, 'node/'. $file['nid'], NULL, NULL, NULL, NULL, TRUE);
    }
    else {
      return $img;
    }
  }
}