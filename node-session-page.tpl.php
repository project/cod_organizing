<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">
 <?php print $conference_header ?>
  <h2 class="title">
    <a href="<?php print $node_url ?>"><?php print $title; ?></a>
  </h2>

  <div class="content">
    <?php print $vote_up_down ?>
    <?php print $short_content; ?>
  </div>
  <?php if ($notes_add_link): ?>
  <div class="add-notes">
  <?php print $notes_add_link ?>
  </div>
  <?php endif; ?>
  
  <?php if ($notes): ?>
  <div class="notes-tite">
  <?php print $notes_title ?>
  </div>
  <div class="notes-body">
  <?php print $notes_body ?>
  </div>
  <div class="notes-edit-link">
  <?php print $notes_edit_link ?>
  </div>
  <?php endif; ?>
  
  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

</div>
