<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">
  
  <?php if ($notes_edit_link): ?>
    <div class="edit-notes">
      <?php print $notes_edit_link; ?>
    </div>
  <?php endif; ?>
  
  <?php if ($notes_body): ?>
    <div class="group-notes">
      <?php print $notes_body; ?>
    </div>
  <?php endif; ?>
  
  <?php if ($message_post_link && $messages): ?>
    <div class="messages-header">
      <h3>
        <?php if ($message_post_link): ?>
          <div class="add-message">
            <?php print $message_post_link; ?>
          </div>
        <?php endif; ?>    
        <?php print t('Messages'); ?>
      </h3>
    </div>
  
    <?php if ($messages): ?>
      <div class="group-messages">
        <?php print $messages; ?>
        <div class="feed-icon"><?php print $messages_feed; ?></div>
      </div>
    <?php endif; ?>
  <?php endif; ?>
  
  <?php if ($tag_cloud): ?>
    <div class="group-tags-header">
      <h3><?php print t('Tags'); ?></h3>
    </div>
    <div class="group-tags">
      <?php print $tag_cloud; ?>
    </div>
  <?php endif; ?>
  
  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>
</div>

<?php print $discussion_title ?>