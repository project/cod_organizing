
$(document).ready(function(){
  $(".scheduled-node .collapser").hide();
  $(".scheduled-node .session-node-title").addClass("green-collapsed");
  $(".scheduled-node .session-node-title").click(function(){
     $(this).siblings(".collapser").toggle();
     $(this).toggleClass("green-expanded");
     $(this).toggleClass("green-collapsed");
     return false;
   });
});

