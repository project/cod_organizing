<?php //print '<pre>'. check_plain(print_r($node)) .'</pre>';?>
<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">
<!--  <div class="green-session-title"><?php print $profile_title ?></div> -->
  <div class="profile-picture"><?php if ($picture) print $picture; ?></div>  
  <div class="summary">
    <div class="rounded-grey-box grey-title"><?php print $first_name .' '. $last_name; ?></div>
    <div class="blue-info"><?php print $info; ?></div>
    <p><?php print $bio ?></p>
  </div>
  
  <div class="my-schedule">
    <?php print $my_schedule; ?>
  </div>
  
  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>
 
</div>
