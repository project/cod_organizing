$(document).ready(function(){
  // we have to explicitly wrap and define background color to prevent safari bug. Hacky but the only way to get this to work in Safari.

  $(".red-header").corner("top cc:#fff");
  $(".green-header").corner("top cc:#fff");

  $(".rounded-black-box").corner("cc:#fff");
  $(".green-session-title").corner("cc:#fff");
  $(".rounded-grey-box").corner("cc:#fff");
  $(".view-all-confirmed-sessions").corner("cc:#fff");
  $(".view-all-proposed-sessions").corner("cc:#fff");

  $("body.groups-directory h1.title").corner("cc:#fff");
  $("body.groups-directory h3").corner("cc:#fff");

  $(".ntype-bio .my-schedule h3").corner("cc:#fff");

  $(".ntype-group .group-tags-header h3").corner("cc:#fff");
  $(".ntype-group .messages-header h3").corner("cc:#fff");
 
  $(".messages").corner("cc:#fff");
  $(".rounded-green-box").corner("cc:#fff");
});