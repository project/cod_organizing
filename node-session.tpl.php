<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">
  <?php if ($page == 0): ?>
      <h2 class="title session-node-title">
        <a href="<?php print $node_url ?>"><?php print truncate_utf8($title, 65, FALSE, TRUE); ?></a>
      </h2>
  <?php endif; ?>
  
  <div class="collapser">

    <div class="content">
      <?php print $vote_up_down ?>
      <?php print $short_content; ?>
      <?php print $read_more ?>
    </div>
    
    <?php if ($links): ?>
      <div class="links">
        <?php print $links; ?>
      </div>
    <?php endif; ?>
  </div>
  <div style="clear:both"></div>
</div>
