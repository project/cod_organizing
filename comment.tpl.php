<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
<?php if ($new != '') { ?><span class="new"><?php print $new; ?></span><?php } ?>
<?php if ($author_picture):?><?php print $author_picture; ?><?php endif; ?>
    <div class="submitted"><?php if($content) { print theme('username', $comment) . t(' said:'); } ?></div>
    <div class="content"><?php print $content; ?></div>
    <div class="links"><?php print $links; ?></div>
</div>
