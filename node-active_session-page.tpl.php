
<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">
  <div class="session-title rounded-green-box">
      <div class="green-session-title">
        <a href="<?php print $node_url ?>"><?php print $title; ?></a>
        <div class="info"><?php print $info; ?></div>  
      </div>
      
      <div class="session-description"><?php print $session_description ?></div>  
  </div>
        
  <div class="rounded-grey-box">
    <div class="notes-link">
      <?php print $notes_link ?>
    </div>  
    <div class="notes-title">
        <h2 class="title"><?php print $notes_title ?></h2>
    </div>
  </div>


  
  <?php if ($notes): ?>
    <div class="notes-body">
      <?php print $notes_body ?>
    </div>
  <?php endif; ?>
  
  
  <div class="rounded-grey-box">
    <?php if ($links_add_link): ?>
      <div class="add-links-link">
        <?php print $links_add_link ?>
      </div>
    <?php endif; ?>
    <div class="session-links-title">
        <h2 class="title"><?php print $links_title ?></h2>
    </div>
  </div>
  
  <?php if ($links_body): ?>
    <div class="links-body">
      <?php print $links_add ?>
      <?php print $links_body ?>   
    </div>
  <?php endif; ?>  
  
   <div class="rounded-grey-box">
    <div class="add-assets-link">
      <?php print $assets_add_link ?>
    </div>
    <div class="session-assets-title">
        <h2 class="title"><?php print $assets_title ?></h2>
    </div>
  </div>
  <div class="attachments">
    <?php print $assets_add ?>
    <?php print $attachments ?>
  </div>
 
  
  <?php if ($discussions_body): ?>
    <div class="rounded-grey-box">
    <div class="add-discussions-link">
      <?php print $discussions_add_link ?>
    </div>
    <div class="discussions-title">
    <h2 class="title"><?php print $discussions_title ?></h2>
    </div>
    </div>
    <div class="discussions-body">
    <?php print $discussions_body ?>
    </div>
  <?php endif; ?>
  
  <?php if ($working_group_body): ?>
    <div class="rounded-grey-box">
    <div class="add-working-group-link">
      <?php print $working_group_add_link ?>
    </div>
    <div class="working-group-title">
    <h2 class="title"><?php print $working_group_title ?></h2>
    </div>
    </div>
    <div class="working-group-body">
    <?php print $working_group_body ?>
    </div>
  <?php endif; ?>

</div>
