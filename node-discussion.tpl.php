<div class="<?php print $node_classes ?>" id="node-<?php print $node->nid; ?>">
  
  <div class="date"><?php print format_date($node->created, 'custom', "F jS, Y") ?></div>
    <h2 class="title">
      <a href="<?php print $node_url ?>"><?php print $title; ?></a>
    </h2>

  
  <div class="content">
    <?php print $content; ?>
  </div>
  
  <span class="submitted"><?php print t(' Posted by ') . theme('username', $node); ?></span

  <?php if (count($taxonomy)): ?>
    <div class="taxonomy"><?php print t(' Tags: ') . $terms ?></div>
  <?php endif; ?>
  
  <?php if ($links): ?>
    <div class="links">
      <?php print $links; ?>
    </div>
  <?php endif; ?>
 
</div>
